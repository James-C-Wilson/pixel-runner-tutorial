# Tutorial for using the Pygame module in Python.  I plan on using additional material and the material here to create my own game.
# Eventually I will try to incorporate an entire DevOps process featuring a wide variety of tools and technologies. 

# tutorial: https://www.youtube.com/watch?v=AY9MnQ4x3zk&t=14s

# Note: I wrote everything out myself for additional practice with the Python syntax.  I already understand all the concepts mentioned in the video to fluency.  I wanted to find a project that I could be passionate about and figured making games, and using DevOps practices and methodologies would be fun!
